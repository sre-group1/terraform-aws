#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

# install nginx
#apt-get update
yum install update
sudo amazon-linux-extras install nginx1 -y
sudo amazon-linux-extras enable nginx1
#apt-get -y install nginx
#yum -y install nginx1

# make sure nginx is started
#service nginx1 start
sudo systemctl start nginx
